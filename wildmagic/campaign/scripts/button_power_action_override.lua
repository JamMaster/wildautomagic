function onInit() 
    math.randomseed(os.time());
end

function action(draginfo)
    PowerManager.performPCPowerAction(draginfo, window.getDatabaseNode(), subroll and subroll[1]);
    
    local probability = parseProbability(Config.WildProbability);

    -- Check if cantrip
    local spellNode = window.getDatabaseNode().getChild("...");
    local spellLevel = DB.getValue(spellNode, "level");
    if spellLevel == nil then
        Debug.console("The used spell has no level, aborting wild magic calculations");
        return;
    end
    local isCantrip = spellLevel <= 0;

    -- Check if cast or effect
    local actionType = DB.getValue(window.getDatabaseNode(), "type");
    local isApplicableForType = (actionType == "cast" or actionType == "effect") and not subroll;

    -- Roll wild magic
    local playerNode = window.getDatabaseNode().getChild(".....");
    if hasWildMagicSurgeFeature(playerNode) and not isCantrip and isApplicableForType and shouldRollWildMagic(probability) then
        local rRoll = { sType = "dice", sDesc = "[WILD MAGIC]", aDice = { "d20" }, nMod = 0 };
        ActionsManager.performAction(draginfo, nil, rRoll);
    end
end
          
function onButtonPress()
    action();
end

function onDragStart(button, x, y, draginfo)
    action(draginfo);
    return true;
end

function hasWildMagicSurgeFeature(node)
    local children = DB.getChildren(node, "featurelist");
    for _, child in pairs(children) do
        if DB.getValue(child, "name") == "Wild Magic Surge" then
            return true;
        end
    end

    return false;
end

function shouldRollWildMagic(probability) 
    local random = math.random();
    Debug.console("Wild magic random is ", random);
    
    return random <= probability;
end

function parseProbability(probSetting)
    if type(probSetting) == "number" then
        local probNumber = tonumber(probSetting);
        if probNumber < 0 then
            return 0;
        elseif probNumber > 100 then
            return 1;
        else
            return probNumber / 100.0;
        end
    else
        return 0.5;
    end
end