-- Odds (in %) of the wild magic roll being performed
-- 0 disables wild magic rolls completely
-- 100 makes the rolls happen with every applicable spell cast
-- Default 50%
WildProbability = 1000;